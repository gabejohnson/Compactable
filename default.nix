{ nixpkgs ? import <nixpkgs> {}, compiler ? "default" }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, stdenv, containers, transformers, vector }:
    mkDerivation {
        pname = "Compactable";
        version = "0.1.0.0";
        src = ./.;
        libraryHaskellDepends = [ base transformers vector containers ];
        description = "A generalization for containers that can be stripped of Nothing";
        license = stdenv.lib.licenses.bsd3;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  drv = haskellPackages.callPackage f {};

in

  if pkgs.lib.inNixShell then drv.env else drv
